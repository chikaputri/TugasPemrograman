import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    // You can add new variables or methods in this class

    public static void main(String[] args) {
        // TODO Complete me!
		Scanner input = new Scanner (System.in);
		TrainCar next = null;
		int counter = 1;
		int sum = input.nextInt();
		input.nextLine();
		for (int i=0; i<sum; i++){
			String[] inputan = input.nextLine().split(",");
			WildCat cat = new WildCat (inputan[0], Double.parseDouble(inputan[1]), Double.parseDouble(inputan[2]));
			if (next == null){
					next = new TrainCar(cat);
			}
			else{
				next = new TrainCar(cat, next);
			}
			if (next.computeTotalWeight() <= 250 && i<sum-1){
				counter++;
			}
			else{
				trainCAW(next, counter);
				counter = 1;
				next = null;
			}
		}
	}
	
	public static void trainCAW (TrainCar train, int counter){
		System.out.println("The train departs to Javari Park");
		System.out.print("[LOCO]<--");
		train.printCar();
		double avgWeight = train.computeTotalMassIndex()/counter;
		String kategori = "*obese*";
		kategori = (avgWeight < 30) ? "*overweight*" : kategori;
		kategori = (avgWeight < 25) ? "*normal*" : kategori;
		kategori = (avgWeight < 18.5) ? "*underweight*" : kategori;
		System.out.printf("Average mass index of all cats: %,.2f%n", avgWeight);
		System.out.printf("In average, the cats in the train are %s", kategori);
	}
}
