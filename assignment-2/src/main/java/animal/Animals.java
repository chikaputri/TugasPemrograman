package animal;

public class Animals {

    protected String name;
	protected String species;
    protected int length;

    public Animals(String name, int length, String species) {
        this.name = name;
        this.length = length;
        this.species = species;
    }

    public String getName() {
        return this.name;
    }

    public int getLength() {
        return this.length;
    }

    public String getSpecies() {
        return this.species;
    }
}