package animal;
import java.util.*;

public class Cat extends Animals {

    public Cat(String name, int length) {
        super(name, length, "cat");
    }

    public void behavior() {
        Scanner input = new Scanner(System.in);
        Random random = new Random();
        String[] randomVoice = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};

        System.out.println("1: Brush the fur 2: Cuddle");
        String pilih = input.nextLine();

        if (pilih.equals("1")) {
            System.out.println("Time to clean " + name + "'s fur");
            System.out.println(name + " makes a voice: Nyaaan...");
        } 
		else if (input.equals("2")) {
            String catVoice = randomVoice[random.nextInt(4)];
            System.out.println(name + " makes a voice: " + catVoice);
        } 
		else {
            System.out.println("You do nothing...");
        }
    }
}