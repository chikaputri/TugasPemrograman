package animal;
import java.util.*;

public class Hamster extends Animals {

    public Hamster(String name, int length) {
        super(name, length, "hamster");
    }

    public void behavior() {
        Scanner input = new Scanner(System.in);

        System.out.println("1: See it gnawing 2: Order to run in hamster wheel");
        String pilih = input.nextLine();

        if (pilih.equals("1")) {
            System.out.println(name + " makes a voice: ngkkrit.. ngkkrrriiit");
        } 
		else if (pilih.equals("2")) {
            System.out.println(name + " makes a voice: trrr... trrr...");
        } 
		else {
            System.out.println("You do nothing...");
        }
    }
}