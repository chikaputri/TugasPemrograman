package animal;
import java.util.*;

public class Lion extends Animals {

    public Lion(String name, int length) {
        super(name, length, "lion");
    }

    public void behavior() {
        Scanner input = new Scanner(System.in);

        System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
        String pilih = input.nextLine();

        if (pilih.equals("1")) {
            System.out.println("Lio is hunting..");
            System.out.println(name + " makes a voice: err...!");
        } 
		else if (pilih.equals("2")) {
            System.out.println("Clean the lion's mane..");
            System.out.println(name + " makes a voice: Hauhhmm!");
        } 
		else if (pilih.equals("3")) {
            System.out.println(name + " makes a voice: HAUHHMM!");
        } 
		else {
            System.out.println("You do nothing...");
        }
    }
}