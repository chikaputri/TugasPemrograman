package animal;
import java.util.*;

public class Parrot extends Animals {

    public Parrot(String name, int length) {
        super(name, length, "parrot");
    }

    public void behavior() {
        Scanner input = new Scanner(System.in);

        System.out.println("1: Order to fly 2: Do conversation");
        String pilih = input.nextLine();

        if (pilih.equals("1")) {
            System.out.println("Parrot " + name + " flies!");
            System.out.println(name + " makes a voice: FLYYYY...");
        } 
		else if (pilih.equals("2")) {
            System.out.print("You say: ");
            String userSay = input.nextLine();
			String parrotSay = userSay.toUpperCase();
            System.out.println(name + " says: " + parrotSay);
        } 
		else {
            System.out.println(name + " says: HM?");
        }
    }
}