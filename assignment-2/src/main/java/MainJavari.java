import animal.*;
import cage.*;
import java.util.*;

public class MainJavari {
    public static Scanner input = new Scanner(System.in);

    public static ArrayList<Animals> catList = new ArrayList<Animals>();   //inisiani arraylist untuk masing-masing hewan
    public  static ArrayList<Animals> lionList = new ArrayList<Animals>();
    public  static ArrayList<Animals> eagleList = new ArrayList<Animals>();
    public  static ArrayList<Animals> parrotList = new ArrayList<Animals>();
    public  static ArrayList<Animals> hamsterList = new ArrayList<Animals>();

    public static Cages catsCages, hamstersCages, parrotsCages, eaglesCages, lionsCages;  //inisiasi kandang
    public static Map<String, ArrayList<Animals>> myAnimals = createMap();  //inisiasi map untuk memetakan arraylist hewan

    public static void main(String[] args) {

        System.out.println("Welcome to Javari Park!");

        inputProcessing();  //memroses segmen pertama
        cageProcessing();   //memroses segmen kedua

        String masukan;
        String species;

        String[] animalsName = {"cat", "eagle", "hamster", "parrot", "lion"};
        Class[] animalClass = {Cat.class, Eagle.class, Hamster.class, Parrot.class, Lion.class};

        while (true) {
            System.out.println("Which animal you want to visit?");
            System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            masukan = input.nextLine();

            if (masukan.equals("99")) {
                System.out.println("Bye fren");
                break;
            }
            else if (Integer.parseInt(masukan) > 5) {
                System.out.println("You do nothing...\n");
                continue;
            }

            species = animalsName[Integer.parseInt(masukan) - 1];

            System.out.print("Mention the name of " + species + " you want to visit: ");
            masukan = input.nextLine();

            boolean check = false;

            Animals animal = null;

            for (Animals animalRn : myAnimals.get(species)) {
                if (animalRn.getName().equals(masukan)) {
                    check = true;
                    animal = animalRn;
                    break;
                }
            }

            if (check) {
                System.out.println("You are visiting " + input + "(" + species + ") now, what would you like to do?");
                if (species.equals("cat"))
                    Cat.class.cast(animal).behavior();
                else if (species.equals("eagle"))
                    Eagle.class.cast(animal).behavior();
                else if (species.equals("hamster"))
                    Hamster.class.cast(animal).behavior();
                else if (species.equals("parrot"))
                    Parrot.class.cast(animal).behavior();
                else if (species.equals("lion"))
                    Lion.class.cast(animal).behavior();
            } else
                System.out.print("There is no " + species + " with that name! ");

            System.out.println("Back to the office!\n");
        }

    }

    public static void inputProcessing() {

        String[] masukan;
        String[] info;
        String[] animalList = {"cat", "lion", "eagle", "parrot", "hamster"};    //inisiasi urutan hewan sesuai input
        int n;  //jumlah hewan

        System.out.println("Input the number of animals");

        for (String animalRn : animalList) {
            System.out.print(animalRn + ": ");
            n = Integer.parseInt(input.nextLine());
            if (n > 0) {
                System.out.println("Provide the information of " + animalRn + "(s): ");
                masukan = input.nextLine().split(",");

                for (int i = 0; i < n; i++) {
                    info = masukan[i].split("\\|");
                    myAnimals.get(animalRn).add(createAnimal(animalRn, info[0], Integer.parseInt(info[1])));
                }
            }
        }

        System.out.println("Animals has been successfully recorded!\n");
    }

    public static void cageProcessing() {

        if (!catList.isEmpty()) {
            catsCages = new Cages(catList, "indoor");
        }
        if (!lionList.isEmpty()) {
            lionsCages = new Cages(lionList, "outdoor");
        }
        if (!eagleList.isEmpty()) {
            eaglesCages = new Cages(eagleList, "outdoor");
        }
        if (!parrotList.isEmpty()) {
            parrotsCages = new Cages(parrotList, "indoor");
        }
        if (!hamsterList.isEmpty()) {
            hamstersCages = new Cages(hamsterList, "indoor");
        }

        Cages.arrangeCages();
    }

    public static Animals createAnimal(String species, String name, int length) {
        if (species.equals("cat"))
            return new Cat(name, length);
        if (species.equals("lion"))
            return new Lion(name, length);
        if (species.equals("eagle"))
            return new Eagle(name, length);
        if (species.equals("parrot"))
            return new Parrot(name, length);
        if (species.equals("hamster"))
            return new Hamster(name, length);
        return null;
    }

    public static Map<String, ArrayList<Animals>> createMap() {
        Map<String, ArrayList<Animals>> map = new HashMap<String, ArrayList<Animals>>();
        map.put("cat", catList);
        map.put("lion", lionList);
        map.put("eagle", eagleList);
        map.put("parrot", parrotList);
        map.put("hamster", hamsterList);

        return map;
    }
}