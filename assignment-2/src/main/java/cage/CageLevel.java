package cage;
import animal.*;
import java.util.*;

class CageLevel {

    private ArrayList<Cage> cages;

    public CageLevel(){
        this.cages = new ArrayList<Cage>();
    }

    public void add(Cage cage){
        cages.add(cage);
    }

    public boolean isEmpty(){
        return cages.isEmpty();
    }

    public ArrayList<Cage> getCages(){
        return cages;
    }

    public void reverse(){
        ArrayList<Cage> newLevel = new ArrayList<Cage>();

        for (int i = cages.size()-1; i >= 0; i--) {
            newLevel.add(cages.get(i));
        }

        cages = newLevel;
    }
}