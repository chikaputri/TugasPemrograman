package cage;
import animal.*;
import java.util.*;

public class Cages {

    private static ArrayList<Cages> allCages = new ArrayList<Cages>();

    private ArrayList<Animals> animals;
    private String cageLocation;
    private String species;
    private CageLevel[] cageLevel = new CageLevel[3];

    public Cages(ArrayList<Animals> animals, String cageLocation) {
        this.animals = animals;
        this.cageLocation = cageLocation;
        this.species = animals.get(0).getSpecies();

        for (int i = 0; i < 3; i++)
            cageLevel[i] = new CageLevel();

        int index = 0;
        int limit = (int) (animals.size() / 3); //maksimum hewan untuk setiap levelnya
        limit = (animals.size() % 3 == 2 && limit != 0) ? limit + 1 : limit;    //perhitungan jika jumlah hewan mod 3 == 2
        limit = (limit == 0) ? 1 : limit;   //case jika jumlah hewan < 3
        int limitCounter = 0;

        for (int i = 0; i < animals.size(); i++){

            Animals animalsRn = animals.get(i);

            cageLevel[index].add(new Cage(animalsRn, cageLocation));

            limitCounter++;

            if (limitCounter >= limit) {
                index = (index + 1) % 3;
                limitCounter = 0;
            }

            if ((i == animals.size()-2) && (animals.size() % limit == 1))    //case jika jumlah hewan mod 3 == 1, dimasukkan ke level 3
                index = 2;
        }

        allCages.add(this); //memasukkan data kandang berlevel ke dalam arraylist dari kandang semua hewan
    }

    public static void arrangeCages() {

        Map<String, Integer> animalsData = new LinkedHashMap<String, Integer>();    //iseng buat sambil belajar
        animalsData.put("cat", 0);
        animalsData.put("lion", 0);
        animalsData.put("eagle", 0);
        animalsData.put("parrot", 0);
        animalsData.put("hamster", 0);

        System.out.println("=============================================");
        System.out.println("Cage arrangement:");

        for (Cages currentCages : allCages) {

            animalsData.put(currentCages.getSpecies(), currentCages.getAnimals().size());   //mengupdate nilai di map

            System.out.println("location: " + currentCages.getLocation());

            currentCages.getLevelsInfo();

            //reverse cages elements
            for (CageLevel levelRn : currentCages.cageLevel) {
                levelRn.reverse();
            }

            //rearrange level
            CageLevel tem1 = currentCages.cageLevel[2], tem2;
            for (int l = 0; l < 3; l++) {
                tem2 = currentCages.cageLevel[l];
                currentCages.cageLevel[l] = tem1;
                tem1 = tem2;
            }

            System.out.println("After rearrangement...");

            currentCages.getLevelsInfo();
        }

        System.out.println("NUMBERS OF ANIMALS:");
        for (String key : animalsData.keySet()) {
            System.out.println(key + ":" + animalsData.get(key));
        }
        System.out.println("\n=============================================");

    }

    public String getLocation() {
        return this.cageLocation;
    }

    public String getSpecies() {
        return this.species;
    }

    public ArrayList<Animals> getAnimals() {
        return this.animals;
    }

    //print setiap level
    public void getLevelsInfo() {
        for (int i = 2; i >= 0; i--) {
            System.out.print("level " + (i + 1) + ":");

            if (!this.cageLevel[i].isEmpty()) {
                for (Cage cageRn : this.cageLevel[i].getCages())
                    System.out.print(cageRn);
            }
            System.out.println();
        }
        System.out.println();
    }
}