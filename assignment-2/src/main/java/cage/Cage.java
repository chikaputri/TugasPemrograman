package cage;
import animal.*;
import java.util.*;

public class Cage {

    private Animals animal;
    private String type = "C";

    public Cage(Animals animal, String location) {
        this.animal = animal;

        if (location.equals("indoor")) {
            if (animal.getLength() < 45){
                this.type = "A";
            }
            else if (animal.getLength() < 60){
                this.type = "B";
            }
        }
        else {
            if (animal.getLength() < 75) {
                this.type = "A";
            }
            else if (animal.getLength() < 91) {
                this.type = "B";
            }
        }
    }

    public String toString() {
        return " " + this.animal.getName() + " (" + this.animal.getLength() + " - " + this.type + "),";
    }
}