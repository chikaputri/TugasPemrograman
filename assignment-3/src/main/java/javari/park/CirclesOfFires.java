package javari.park;

import java.util.*;

public class CirclesOfFires extends Attractions{
    
    private static String[] validAnimals = {"Lion", "Whale", "Eagle"};
    private ArrayList<String> listOfAnimals = new ArrayList<String>();

    public CirclesOfFires(String name, String type){
        super(name, type);
    }

    public static boolean checkValidity(String type){
        for(int i = 0; i < validAnimals.length; i++){
            if(type.equals(validAnimals[i])){
                return true;
            }
        }
        return false;
    }
}