package javari.park;

import java.util.*;

public class Visitor implements Registration{

    private String name;
    private int id;
    private int regisCount = 0;
    private static ArrayList<Registration> listOfVisitors = new ArrayList<Registration>();
    private ArrayList<SelectedAttraction> listOfSelectedAttractions = new ArrayList<SelectedAttraction>();


    public Visitor(String name){
        this.name = name;
        this.id = regisCount;
        listOfVisitors.add(this);
        regisCount += 1;
    }

    public int getRegistrationId(){
        return this.id;
    }

    public String getVisitorName(){
        return this.name;
    }

    public void setVisitorName(String name){
        this.name = name;
    }

    public List<SelectedAttraction> getSelectedAttractions(){
        return listOfSelectedAttractions;
    }

    public boolean addSelectedAttraction(SelectedAttraction selected){
        listOfSelectedAttractions.add(selected);
        return true;
    }

    public static Registration find(String name){
        for(int i = 0; i < listOfVisitors.size(); i++){
            if(listOfVisitors.get(i).getVisitorName().equalsIgnoreCase(name)){
                return listOfVisitors.get(i);
            }
        }
        return null;
    }
}