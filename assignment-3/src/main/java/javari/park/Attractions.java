package javari.park;

import java.util.*;
import javari.park.*;
import javari.animal.*;
import javari.park.*;

public class Attractions implements SelectedAttraction{
    
    private String name;
    private String type;
    private List<Animal> performers = new ArrayList<Animal>();
    private static ArrayList<Attractions> listOfAttractions = new ArrayList<Attractions>();
    private static String[] validCategories = {"Explore the Mammals", "Reptillian Kingdom", "World of Aves"};
    private static String[] validAttractions = {"Circles of Fires", "Counting Masters", "Dancing Animals", "Passionate Coders"};
    private ArrayList<Attractions> attractions = new ArrayList<Attractions>();
    
    public Attractions(){
		
    }
	
    public Attractions(String name, String type) {
        this.name = name;
        this.type = type;
        listOfAttractions.add(this);
    }
    public static String[] getValidCategories(){
        return validCategories;
    }

    public static String[] getValidAttractions(){
        return validAttractions;
    }
    
    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public List<Animal> getPerformers() {
        return performers;
    }

    public boolean addPerformer(Animal performer) {
        if (performer.isShowable()) {
            performers.add(performer);
            return true;
        }
        return false;
    }

    public static ArrayList<Attractions> getAttractionsList() {
        return listOfAttractions;
    }

    public ArrayList<Attractions> findAnimal(String animal){
        //System.out.println(animal);
        for(int i = 0; i < listOfAttractions.size(); i++){
            if(listOfAttractions.get(i).getType().equalsIgnoreCase(animal)){
                attractions.add(listOfAttractions.get(i));
            }
        }
        return attractions;
    }
}