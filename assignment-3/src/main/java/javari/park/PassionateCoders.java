package javari.park;

import java.util.*;

public class PassionateCoders extends Attractions{
    
    private static String[] validAnimals = {"Hamster", "Cat", "Snake"};
    private ArrayList<String> listOfAnimals = new ArrayList<String>();

    public PassionateCoders(String name, String type){
        super(name, type);
    }

    public static boolean checkValidity(String type){
        for(int i = 0; i < validAnimals.length; i++){
            if(type.equals(validAnimals[i])){
                return true;
            }
        }

        return false;
    }

}