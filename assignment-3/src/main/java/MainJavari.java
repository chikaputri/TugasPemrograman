import java.io.BufferedReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import javari.park.*;
import javari.reader.*;
import javari.park.*;
import javari.animal.*;
import javari.writer.*;
import java.io.IOException;
import java.util.*;

public class MainJavari {

    static CsvReader a;
    static CsvReader b;
    static CsvReader c;
    static Scanner masukan = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.print("Welcome to Javari Park Festival - Registration Service!\n\n... Opening default section database from data.");
        String directory = System.getProperty("user.dir") + "\\data";

        while(true){
            try{
                //membuat tiga objek csv reader berdasarkan file yg dibaca
                a = new AnimalCategories(Paths.get(directory, "animals_categories.csv"));
                b = new AnimalAttractions(Paths.get(directory, "animals_attractions.csv"));
                c = new AnimalRecords(Paths.get(directory, "animals_records.csv"));
                System.out.println("\n... Loading... Success... System is populating data...\n");
                break;
            }
			catch (IOException ex){
                System.out.print("... File not found or incorrect file!\n\nPlease provide the source data path: ");
                directory = masukan.nextLine();
                directory = directory.replace("\\", "\\\\");
            }
        }

        //memanggil method-method dari setiap objek untuk menghitung jumlah data yang valid & invalid
        System.out.println("Found " + a.countValidRecords() + " valid sections and " + a.countInvalidRecords() + " invalid sections");
        System.out.println("Found " + b.countValidRecords() + " valid attractions and " + b.countInvalidRecords() + " invalid attractions");
        System.out.println("Found " + a.countValidRecords() + " valid categories and " + a.countInvalidRecords() + " invalid categories");
        System.out.println("Found " + c.countValidRecords() + " valid records and " + c.countInvalidRecords() + " invalid records");
        
        mainMenu();

    }

    //membuat method print output pertama
    public static void mainMenu(){

        System.out.print("\nWelcome to Javari Park Festival - Registration Service!\n\nPlease answer the questions by typing the number. Type # if you want to return to the previous menu\n");
        nextMenu();
    }

    //membuat method section
    public static void nextMenu(){
        while(true){

            System.out.println("\nJavari Park has 3 sections:");
            System.out.println("1. Explore the Mammals");
            System.out.println("2. World of Aves");
            System.out.println("3. Reptilian Kingdom");
            System.out.print("Please choose your preferred section (type the number): ");
            String input = masukan.nextLine();
            switch(input){
                case "1":
                    sectionMenu("Explore the Mammals");
                    break;
                case "2":
                    sectionMenu("World of Aves");
                    break;
                case "3":
                    sectionMenu("Reptilian Kingdom");
                    break;
                case "#":
                    System.out.println("Farewell!");
                    System.exit(0);
                default:
                    System.out.println("Please enter a valid command!");
                    continue;
            }
        }

    }

    //membuat method untuk memilih binatang
    public static void sectionMenu(String section){
        while(true){
            System.out.println("\n--" + section + "--");
            switch(section){
                case "Explore the Mammals":
                int j = 1;
                for(int x = 0; x < Mammals.getMamalia().length; x++){
                    System.out.println(j + ". " + Mammals.getMamalia()[x]);
                    j++;
                }
                System.out.print("Please choose your preferred animals (type the number): ");
                String input = masukan.nextLine();
                    switch(input){
                        case "#":
                            nextMenu();
                            break;
                        case "1":
                            attractionMenu("Cat");
                            break;
                        case "2":
                            attractionMenu("Whale");
                            break;
                        case "3":
                            attractionMenu("Hamster");
                            break;
                        case "4":
                            attractionMenu("Lion");
                            break;
                        default:
                            System.out.println("Please enter a valid command!");
                            continue;
                    }
                    break;
                case "World of Aves":
                    int k = 1;
                    for(int x = 0; x < Aves.getAves().length; x++){
                        System.out.println(k + ". " + Aves.getAves()[x]);
                        k++;
                    }
                    System.out.print("Please choose your preferred animals (type the number): ");
                    String input2 = masukan.nextLine();
                    switch(input2){
                        case "#":
                            nextMenu();
                            break;
                        case "1":
                            attractionMenu("Parrot");
                            break;
                        case "2":
                            attractionMenu("Eagle");
                            break;
                        default:
                            System.out.println("Please enter a valid command!");
                            continue;
                    }
                    break;
                case "Reptilian Kingdom":
                    int i = 1;
                    for(int x = 0; x < Reptiles.getReptilia().length; x++){
                        System.out.println(i + ". " + Reptiles.getReptilia()[x]);
                        i++;
                    }
                    System.out.print("Please choose your preferred animals (type the number): ");
                    String input3 = masukan.nextLine();
                    switch(input3){
                        case "#":
                            nextMenu();
                            break;
                        case "1":
                            attractionMenu("Snake");
                            break;
                        default:
                            System.out.println("Please enter a valid command!");
                            continue;
                    }
                    break;
            }

        }
    }

    //membuat method untuk memilih atraksi
    public static String attractionMenu(String animal){
        while(true){
            int i = 1;
            System.out.println("\n---" + animal + "---");
            System.out.println("Attractions by " + animal + ":");
            Attractions atraksi = new Attractions();
            int size = atraksi.findAnimal(animal).size();
            if(size == 0){
                return "Unfortunately, no " + animal + " can perform any attraction, please choose other animals";
            }else{
                for(int x = 0; x < size; x++){
                    System.out.println(i + ". " + atraksi.findAnimal(animal).get(x).getName());
                    i += 1;
                }
    
                System.out.print("Please choose your preferred attractions (type the number): ");
                String input = masukan.nextLine();
                if(input.equalsIgnoreCase("#")){
                    if(Arrays.asList(Mammals.getMamalia()).contains(animal)){
                        sectionMenu("Explore the Mammals");
                    }else if(Arrays.asList(Aves.getAves()).contains(animal)){
                        sectionMenu("World of Aves");
                    }else if(Arrays.asList(Reptiles.getReptilia()).contains(animal)){
                        sectionMenu("Reptillian Kingdom");
                    }
                    break;
                }else{
                    try{
                        int angka = Integer.parseInt(input);
                        if(angka < i){
                            lastCheck(atraksi.findAnimal(animal).get(angka));
                            break;
                        }else{
                            System.out.println("Please enter a valid command!");
                            continue;
                        }
                    }catch(NumberFormatException a){
                        System.out.println("Please enter a valid command!");
                        continue;
                    }
                }
    
            }

        }
        return "";

    }

    //membuat method untuk memeriksa data
    public static void lastCheck(Attractions atraksi){
        while(true){
            
            System.out.print("\nWow, one more step,\nplease let us know your name: ");
            String customerName = masukan.nextLine();
            Registration me = Visitor.find(customerName);
            if(me == null){
                me = new Visitor(customerName);
            }
            System.out.println("\nYeay, final check!\nHere is your data, and the attraction you chose:");
            System.out.println("Name: " + customerName);
            System.out.println("Attractions: " + atraksi.getName() + " -> " + atraksi.getType());
            String hasil = "";
            for(int x = 0; x < atraksi.getPerformers().size(); x++){
                String nama =  atraksi.getPerformers().get(x).getName();
                if(nama.length() == 1){
                    nama = nama.toUpperCase();
                }else{
                    nama = nama.substring(0,1).toUpperCase() + nama.substring(1);
                }
                if(x != atraksi.getPerformers().size()-1){
                    hasil += nama + ", ";
                }else{
                    hasil += nama;
                }
            }
            System.out.println("With: " + hasil);
            System.out.print("\nIs the data correct? (Y/N): ");
            String choice = masukan.nextLine();
            switch(choice){
                case "Y":
                    me.addSelectedAttraction(atraksi);
                    System.out.print("\nThank you for your interest. Would you like to register to other attractions? (Y/N): ");
                    String secondchoice = masukan.nextLine();
                    switch(secondchoice){
                        case "Y":
                            nextMenu();
                            break;
                        case "N":
                                String hasil1 = me.getVisitorName().replace(" ", ",");
                                System.out.println("... End of program, write to registration_" + hasil1 + ".json ...");
                                System.exit(0);
                            break;  
                        default:
                            System.out.println("Please enter a valid command!");
                            continue;
                    }
                case "N":
                    System.out.println("Please re-fill the data!");
                    nextMenu();
                    break;
                default:
                    System.out.println("Please enter a valid command!");
                    continue;
                }
		}
	}
}