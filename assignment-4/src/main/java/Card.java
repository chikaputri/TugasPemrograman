import javax.swing.*;
import java.awt.*;

public class Card extends JButton{
    private int id;
    private boolean matched = false;
    private ImageIcon cover = scaleIcon(new ImageIcon(this.getClass().getResource("start.png")));
    private ImageIcon selectedIcon;

    public Card(int id, ImageIcon pic) {
    	this.id = id;
    	selectedIcon = pic;
    	cover();
    }
    
    public void cover() {
    	setIcon(cover);
    	setEnabled(true);
    	matched = false;
    	
    }

    public int getId(){
        return this.id;
    }


    public void setMatched(boolean matched){
        this.matched = matched;
    }

    public boolean getMatched(){
        return this.matched;
    }
    
    
    public static ImageIcon scaleIcon(ImageIcon icon) {
    	return new ImageIcon(icon.getImage().getScaledInstance(100,  100,  Image.SCALE_DEFAULT));
    }
    
    public void matchedIcon () {
    	setEnabled(false);
    	matched = true;
    }
    
    public void clickedPicture() {
    	setIcon(selectedIcon);
    }
}
