
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
@SuppressWarnings("serial")
public class Board extends JFrame{


    private List<Card> cards;
    private Card selectedCard;
    private Card c1;
    private Card c2;
    private Timer t;
    private List<Card> cardsList = new ArrayList<Card>();
    private int tries = 0;
    
    public Board(){

        int pairs = 18;
        for (int i=1; i <= pairs; i++){
            Card c = new Card(i, Card.scaleIcon(new ImageIcon(i + ".jpg")));
            c.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae){
                    selectedCard = c;
                    selectedCard.clickedPicture();
                    flipCard();
                    
                }
            });
            cardsList.add(c);
        }
        
        for (int i=1; i <= pairs; i++){
            Card c = new Card(i, Card.scaleIcon(new ImageIcon(i + ".jpg")));
            c.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae){
                    selectedCard = c;
                    selectedCard.clickedPicture();
                    flipCard();
                    
                }
            });
            cardsList.add(c);
        }
        
        Collections.shuffle(cardsList);
        
        this.cards = cardsList;
        //set up the timer
        t = new Timer(500, new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                checkCards();
            }
        });

        t.setRepeats(false);

        
        JPanel cardPanel = new JPanel(new GridLayout(6, 6));
        for (Card c : cards){
            cardPanel.add(c);
        }
        
        JPanel bottomPanel = new JPanel();
        
        JButton playAgain = new JButton("Play again?");
        bottomPanel.add(playAgain);
        playAgain.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
            	restart();
            }
        });
        
        
        JButton exit = new JButton("Exit");
        bottomPanel.add(exit);
        exit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
            	System.exit(0);
                
            }
        });
        
        
        JLabel attempt = new JLabel();
        attempt.setText("play attempt = " + this.tries);
        bottomPanel.add(attempt);

      //set up the board itself
        Container pane = getContentPane();
        pane.add(cardPanel, BorderLayout.NORTH);
        pane.add(bottomPanel, BorderLayout.SOUTH);
        
      
        setTitle("Match-Pair Memory Game");
    }

    public void flipCard(){
        if (c1 == null && c2 == null){
            c1 = selectedCard;
        }

        if (c1 != null && c1 != selectedCard && c2 == null){
            c2 = selectedCard;
            t.start(); 

        }

    }

    public void checkCards(){
        if (c1.getId() == c2.getId()){//match condition
            c1.setEnabled(false); //disables the button
            c2.setEnabled(false);
            c1.setMatched(true); //flags the button as having been matched
            c2.setMatched(true);
            if (this.isGameWon()){
                JOptionPane.showMessageDialog(this, "You win!");
            }
        }

        else{
            c1.cover();
            c2.cover();
        }
        c1 = null; //reset c1 and c2
        c2 = null;
        this.tries += 1;
    }

    public boolean isGameWon(){
        for(Card c: this.cards){
            if (c.getMatched() == false){
                return false;
            }
        }
        return true;
    }
    
    public void restart() {
    	Container rePane = getContentPane();
    	JPanel restartGame = new JPanel(new GridLayout(6, 6));
    	Collections.shuffle(cards);
    	for(Card c: cards){
            c.setMatched(false);
            c.cover();
            restartGame.add(c);
        }
        rePane.add(restartGame);
        setContentPane(rePane);
        
        tries = 0;
    }
}